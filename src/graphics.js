/**
@file
@brief This file contains all of the logic related to updating the display.

The 3D world is rendered WITHOUT using a depth buffer. This is possible due to
a few "limitations" that we know the world will have:

+ The world is composed of cuboids.
+ The position of each cuboid is restricted to a fixed "grid" position.

By carefully looping through the grid of cubes in the right order and ignoring
any "hidden" faces (i.e. faces obscured by a neighbouring cuboid), we can draw
the cuboids so that a simple "painters algorithm" approach is good enough.
*/

(function () {
  "use strict";

  // NOTE: Tweak this to increase/decrease the effect of weapon-bobbing:
  var MAX_WEAPON_BOB = 2;

  var game_camera;
  var canvas;
  var ctx;
  var shotgun;

  // This is called once, when the program first starts:
  window.gfxInit = function() {
    game_camera = new GameCamera();

    canvas = document.querySelector("canvas");
    ctx = canvas ? canvas.getContext("2d") : null;
    if (!ctx) { return false; }

    // Disable anti-aliasing:
    ctx.mozImageSmoothingEnabled = false;
    ctx.webkitImageSmoothingEnabled = false;
    ctx.msImageSmoothingEnabled = false;
    ctx.imageSmoothingEnabled = false;

    shotgun = document.getElementById("hud-shotgun");
    ctx.font = "6px monospace";
    return true;
  }

  // This is called once per game loop:
  window.gfxDraw = function(actor, timer) {
    var gfx_w = canvas.width;
    var gfx_h = canvas.height;

    if (actor) {
      game_camera.position[0] = actor.position[0];
      game_camera.position[1] = actor.position[1];
      game_camera.position[2] = actor.position[2] + 0.4;
      game_camera.setAngle(actor.angle);
    }

    // Prepare to render 3D - see `camera.js` for more information:
    ctx.setTransform(1, 0, 0, 1, 0, 0);
    ctx.translate(gfx_w/2, gfx_h/2);
    ctx.scale(gfx_w/2, gfx_w/2);

    // Draw a "background":
    ctx.fillStyle = "#666"; ctx.fillRect(-1, -1, 2, 1);
    ctx.fillStyle = "#386"; ctx.fillRect(-1,  0, 2, 1);

    // Draw the cubes. The looping order depends on the current view angle:
    var loop_though_y_first =
        (game_camera.angle > Math.PI*0.25 && game_camera.angle < Math.PI*0.75) ||
        (game_camera.angle > Math.PI*1.25 && game_camera.angle < Math.PI*1.75);
    loop_though_y_first ? loopThroughY(-1) : loopThroughX(-1);

    // Prepare to render 2D, using a "virtual" 320x240 resolution:
    ctx.setTransform(1, 0, 0, 1, 0, 0);
    ctx.scale(gfx_w / 320, gfx_h / 240);

    // Draw the player weapon:
    if (actor) {
      // Calculate weapon bob:
      var sway = (Math.abs(actor.velocity[0]) + Math.abs(actor.velocity[1])) * 1000;
      if (sway > MAX_WEAPON_BOB) { sway = MAX_WEAPON_BOB; }
      var sway_x = Math.sin(timer * 0.007) * sway;
      var sway_y = Math.cos(timer * 0.014) * sway;
      // Draw the HUD weapon:
      ctx.drawImage(shotgun, 160 + sway_x, 240 - shotgun.height + MAX_WEAPON_BOB - sway_y);
    }

    // Debug info:
    ctx.fillStyle = "#FFF";
    ctx.fillText(String(calcFPS(timer)) + " FPS", 2, 238);
  }

  // Calculate the frames-per-second based on the time since the last check:
  var old_timer;
  function calcFPS(timer) {
    var diff = timer - (old_timer || timer);
    old_timer = timer;
    return Math.round(diff ? 1000 / diff : 1000);
  }

  // Loop through the Y axis, possibly in reverse order:
  function loopThroughX(y) {
    var r =  game_camera.angle < Math.PI * 0.5 || game_camera.angle > Math.PI * 1.5;
    for (var x = r ? game_world[0].length - 1 : 0; r ? x >= 0 : x < game_world[0].length; r ? --x : ++x) {
      if (y < 0) {
        loopThroughY(x);
      } else {
        drawCube(x, y);
        drawSprites(x + (r ? 1 : -1), y);
      }
    }
  }

  // Loop through the X axis, possibly in reverse order:
  function loopThroughY(x) {
    var r = game_camera.angle < Math.PI;
    for (var y = r ? game_world.length - 1 : 0; r ? y >= 0 : y < game_world.length; r ? --y : ++y) {
      if (x < 0) {
        loopThroughX(y);
      } else {
        drawCube(x, y);
        drawSprites(x, y + (r ? 1 : -1));
      }
    }
  }

  // Draw a 3D cube at the specified position, hiding any "obscured" faces:
  function drawCube(x, y) {
    if (getTile(x, y)) { return; }

    // Hidden surface removal needs to be done relative to the camera position:
    var vx = x - game_camera.position[0];
    var vy = y - game_camera.position[1];

    // Draw each side of the cube, skipping any "obscured" faces:
    if (vx < +0.5 && getTile(x-1, y)) { drawWall(x, y, 'x-'); }
    if (vx > -0.5 && getTile(x+1, y)) { drawWall(x, y, 'x+'); }
    if (vy < +0.5 && getTile(x, y-1)) { drawWall(x, y, 'y-'); }
    if (vy > -0.5 && getTile(x, y+1)) { drawWall(x, y, 'y+'); }
  }

  function drawWall(x, y, face) {
    // TODO: In the future, alternative wall "textures" will be allowed...
    var polygons = [
      { rgb: "#8AC", vertices: [[-0.5, 0], [0.5, 0], [0.5, 1], [-0.5, 1]] },
      { rgb: "#ACE", vertices: [[-0.4, 0.1], [0.3, 0.1], [-0.4, 0.8]] },
      { rgb: "#ACE", vertices: [[ 0.4, 0.9], [0.4, 0.2], [-0.3, 0.9]] },
    ];

    // Loop through each polygon of the wall "texture" and transform/draw it:
    polygons.forEach(function (polygon) {
      var args = [polygon.rgb];
      polygon.vertices.forEach(function (v) {
        switch (face) {
          case "x-": v = [x - 0.50, y + v[0], 0 + v[1]]; break;
          case "x+": v = [x + 0.50, y - v[0], 0 + v[1]]; break;
          case "y-": v = [x - v[0], y - 0.50, 0 + v[1]]; break;
          case "y+": v = [x + v[0], y + 0.50, 0 + v[1]]; break;
        }
        args.push(game_camera.screenSpace(v[0], v[1], v[2]));
      });
      drawPoly.apply(this, args);
    });
  }

  // Draw an n-point polygon:
  function drawPoly(rgb) {
    var v = arguments;
    // Ignore any polygons where all vertices are "behind" the camera:
    for (var hide = 0, i = 1; i < v.length; ++i) { hide += v[i][2] <= 0.25; }
    if (hide == v.length - 1) { return; }
    // Draw the polygon by using each argument (except the first) as a vertex:
    ctx.fillStyle = rgb;
    ctx.beginPath();
    ctx.moveTo(v[1][0], v[1][1]);
    for (var i = 2; i < v.length; ++i) { ctx.lineTo(v[i][0], v[i][1]); }
    ctx.closePath();
    ctx.fill();
  }

  // Draw all actors present on a specific cell.
  function drawSprites(cell_x, cell_y) {
    // If this cell is a solid wall, ignore it:
    if (getTile(cell_x, cell_y)) { return; }
    // TODO: This could be more efficient - no need to loop over EVERYTHING!
    game_objects.forEach(function(actor) {
      var image = actor.getSprite();
      if (!image) { return; }

      // Ignore any actors that aren't standing on this cell:
      if (cell_x != Math.round(actor.position[0])) { return; }
      if (cell_y != Math.round(actor.position[1])) { return; }

      var actor_size = actor.getSize();

      // Convert the 3D actor position into a 2D screen-space coordinate:
      var xyz = game_camera.screenSpace(
        actor.position[0],
        actor.position[1],
        actor.position[2] + actor_size[2]);

      // If the actor is "in front of" the screen, draw it:
      if (xyz[2] > 0.001) {
        var w = actor_size[0] / xyz[2];
        var h = actor_size[2] / xyz[2];
        ctx.drawImage(image, xyz[0] - (w/2), xyz[1], w, h);
      }
    });
  }

})();

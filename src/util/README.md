PS4 Browser Gamepad Support
===========================

Basic support for getting the state of the pad in the PS4 browser. Tested with
v6.51 of the system software.

How it works
------------

The PS4 browser treats the left stick and X button as a mouse; everything else
is handled as though it were a key on a keyboard. These "mouse" and "keyboard"
events can be detected via Javascipt event handlers, allowing us to figure out
the state of the PS4 pad based on which "keys" or "mouse events" are used.

How to use
----------

Simply include `ps4.js` like so:

    <script src="ps4.js"></script>

You now have access to the following methods:

### `util.ps4.browser()`

Returns `true` if this appears to be the PS4 browser, or `false` if not.

### `util.ps4.startPad()`

Sets up various callbacks to track the state of the PS4 pad.

### `util.ps4.stopPad()`

Clears any previously-set-up callbacks.

### `util.ps4.getPad()`

Gets the current state of a PS4 pad as a `Gamepad`-like Javascript object.

Note that this will only update after `startPad()` has been called, and ceases
do so once `stopPad()` is called.

Limitations
-----------

Some things to bear in mind if you plan to target the PS4 browser:

+ There's no way to prevent "default" button actions like R3 zooming in, since
  `event.preventDefault()` has no effect.
+ The `X` button may not work properly unless the cursor is pointing at a HTML
  element, such as a `<canvas>` tag, since the PS4 treats it as a mouse-click.
+ The Javascript events for circle/square/triangle/options are unreliable.
+ The left stick stops working if the cursor hits the edge of the screen.
+ There's no way to detect the position of the right stick at all.
+ There's no way to detect the Share, PSLOGO, or touchpad "press-in" buttons.
+ There's no way to detect touchpad "touch/drag" events (as far as I know).

**tl;dr:** The left/right/down directional buttons + L1/R1/X are the safest to
use if you want reliable input without side-effects.

Miscellaneous notes
-------------------

Unrelated to pad input, the PS4 browser does support the "fullscreen" API:

<https://developer.mozilla.org/en-US/docs/Web/API/Fullscreen_API>

However, it relies on webkit-prefixed versions of the standard methods, so you
will need to use `canvas.webkitRequestFullscreen()` instead of the more-common
`canvas.requestFullscreen()` method.

License
-------

Copyright 2019 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

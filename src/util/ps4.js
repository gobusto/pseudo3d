(function () {
  "use strict";

  // The PS4 uses the following `event.keyIdentifier` values for pad buttons:
  var PS4_BUTTON_UP = "Up"
  var PS4_BUTTON_DOWN = "Down"
  var PS4_BUTTON_LEFT = "Left"
  var PS4_BUTTON_RIGHT = "Right"
  var PS4_BUTTON_CIRCLE = "ESC"
  var PS4_BUTTON_TRIANGLE = "F1"
  var PS4_BUTTON_SQUARE = "F2"
  var PS4_BUTTON_OPTIONS = "F3"
  var PS4_BUTTON_CROSS = "CROSS" // Special case - it's the left mouse button!
  var PS4_BUTTON_L1 = "F5"
  var PS4_BUTTON_R1 = "F6"
  var PS4_BUTTON_L2 = "F7"
  var PS4_BUTTON_R2 = "F8"
  var PS4_BUTTON_L3 = "F9"
  var PS4_BUTTON_R3 = "F10"

  // This tracks the state of buttons on the pad:
  var buttons = {}

  // This tracks the state of the left stick:
  var leftStick = { x: 0, y: 0 }

  // This updates the state of the left stick based on mouse movement details:
  function updateLeftStick(event) {
    if (leftStick.data) {
      leftStick.x = (event.x - leftStick.data.x) / 10
      leftStick.y = (event.y - leftStick.data.y) / 10
      leftStick.x = Math.min(Math.max(leftStick.x, -1), 1)
      leftStick.y = Math.min(Math.max(leftStick.y, -1), 1)
    }

    leftStick.data = { x: event.x, y: event.y, timeStamp: Number(new Date()) }
  }

  // This updates most buttons in response to a keyup/down event:
  function updateButton(event) {
    buttons[event.keyIdentifier] = (event.type == "keydown")
  }

  // This handles the cross button via mouseup/mousedown events:
  function updateCross(event) {
    buttons["CROSS"] = (event.type == "mousedown")
  }

  // See <https://developer.mozilla.org/en-US/docs/Web/API/Gamepad/buttons>
  function gamepadButtonFrom(value) {
    return value ? { pressed: true, value: 1 } : { pressed: false, value: 0 }
  }

  // Another file may have defined this for us, but if not...
  if (!window.util)
    window.util = {}

  // This is our public interface:
  window.util.ps4 = {
    // Determine if we're using the PS4 browser.
    browser: function() {
      return navigator.userAgent.indexOf("PlayStation 4") >= 0
    },
    // Start tracking button presses on the pad:
    startPad: function() {
      document.body.addEventListener("keyup", updateButton, true)
      document.body.addEventListener("keydown", updateButton, true)
      document.body.addEventListener("mouseup", updateCross, true)
      document.body.addEventListener("mousedown", updateCross, true)
      document.body.addEventListener("mousemove", updateLeftStick, true)
    },
    // Stop tracking button presses on the pad:
    stopPad: function() {
      document.body.removeEventListener("keyup", updateButton, true)
      document.body.removeEventListener("keydown", updateButton, true)
      document.body.removeEventListener("mouseup", updateCross, true)
      document.body.removeEventListener("mousedown", updateCross, true)
      document.body.removeEventListener("mousemove", updateLeftStick, true)
    },
    // Get the current state of the pad in the same format as a "normal" one:
    getPad: function() {
      var timeStamp = Number(new Date())

      // If it's been too long since the last left-stick update, ignore it:
      var okay = leftStick.data && leftStick.data.timeStamp > timeStamp - 20

      // See <https://developer.mozilla.org/en-US/docs/Web/API/Gamepad>
      return {
        axes: okay ? [leftStick.x, leftStick.y, 0, 0] : [0, 0, 0, 0],
        buttons: [
          gamepadButtonFrom(buttons[PS4_BUTTON_CROSS]),
          gamepadButtonFrom(buttons[PS4_BUTTON_CIRCLE]),
          gamepadButtonFrom(buttons[PS4_BUTTON_SQUARE]),
          gamepadButtonFrom(buttons[PS4_BUTTON_TRIANGLE]),
          gamepadButtonFrom(buttons[PS4_BUTTON_L1]),
          gamepadButtonFrom(buttons[PS4_BUTTON_R1]),
          gamepadButtonFrom(buttons[PS4_BUTTON_L2]),
          gamepadButtonFrom(buttons[PS4_BUTTON_R2]),
          gamepadButtonFrom(false), // Share ("Select" on the PS3)
          gamepadButtonFrom(buttons[PS4_BUTTON_OPTIONS]),
          gamepadButtonFrom(buttons[PS4_BUTTON_L3]),
          gamepadButtonFrom(buttons[PS4_BUTTON_R3]),
          gamepadButtonFrom(buttons[PS4_BUTTON_UP]),
          gamepadButtonFrom(buttons[PS4_BUTTON_DOWN]),
          gamepadButtonFrom(buttons[PS4_BUTTON_LEFT]),
          gamepadButtonFrom(buttons[PS4_BUTTON_RIGHT]),
          gamepadButtonFrom(false) // PSLOGO
        ],
        connected: true,
        mapping: "standard",
        timeStamp: timeStamp
      }
    }
  }
})();

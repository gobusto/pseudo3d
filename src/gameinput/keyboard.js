(function () {
  "use strict";

  // This represents the current state of the keyboard. Keys are represented as
  // `{ "ARROWUP": true, "W": false }` so unknown values are implicitly `null`.
  var state = {}

  // This determines whether we have exclusive control over keyboard events. It
  // may be useful to prevent arrow keys from scrolling the page, but note that
  // it will also prevent you from pressing F12 to open the browser devtools!
  var exclusive = false

  // This updates the state in response to a keyup/down event:
  function updateState(event) {
    state[event.key.toUpperCase()] = (event.type == "keydown")
    if (exclusive) event.preventDefault()
    return false
  }

  // Another file may have defined this for us, but if not...
  if (!window.gameInput)
    window.gameInput = {}

  // This is our public interface:
  window.gameInput.keyboard = {
    // Attach listeners to a given DOM element, such as `document.body`:
    attach: function(element) {
      element.addEventListener("keyup", updateState, true)
      element.addEventListener("keydown", updateState, true)
    },
    // Clear previously-attached listeners for a given DOM element:
    clear: function(element) {
      element.removeEventListener("keyup", updateState, true)
      element.removeEventListener("keydown", updateState, true)
    },
    // These functions enable or disable exclusive access to keyboard events:
    enableExclusiveMode: function () { exclusive = true },
    disableExclusiveMode: function () { exclusive = false },
    // Get the current state of the keyboard:
    getState: function() { return state }
  }
})();

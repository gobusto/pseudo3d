(function () {
  "use strict";

  // Another file may have defined this for us, but if not...
  if (!window.gameInput)
    window.gameInput = {}

  // This is our public interface:
  window.gameInput.joypad = {
    // Determine which type of joypad we have.
    //
    // TODO: Try all kinds out on Windows 10.
    //
    // Chrom(e/ium) detects common pads quite well, and automatically maps them
    // to the "standard" layout, but Firefox just passes them through directly,
    // so games may need to use different axis/button indices for consistency.
    guessType: function(joypad) {
      // Debian / Chromium / 360, PS3, and PS4 pads:
      if (joypad.mapping == "standard") return "standard"
      // Debian / Firefox / 360 pad:
      else if (joypad.id.indexOf("360") >= 0) return "360"
      // Debian / Firefox / PS3 pad:
      else if (joypad.id.indexOf("PLAYSTATION(R)3") >= 0) return "ps3"
      // Debian / Firefox / PS4 pad:
      else if (joypad.id.indexOf("Sony") >= 0) return "ps4"
      // Anything else is unrecognised:
      return "unknown"
    },
    // Get the value of the right-stick X axis:
    rightStickX: function(joypad) {
      if (this.guessType(joypad) == "standard") return joypad.axes[2]
      return joypad.axes[3]
    },
    // Get the value of the right-stick Y axis:
    rightStickY: function(joypad) {
      if (this.guessType(joypad) == "standard") return joypad.axes[3]
      return joypad.axes[4]
    },
    // Get the current state of the first attached joypad we find.
    //
    // See http://stackoverflow.com/a/10940236/4200092
    getState: function() {
      var devices = (navigator.getGamepads && navigator.getGamepads()) || []
      for (var i = 0; i < devices.length; ++i)
        if (devices[i] && devices[i].connected)
          return devices[i]
      return null
    }
  }
})();

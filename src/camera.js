/**
@file
@brief This file contains all of the logic related to the 3D camera system.

How coordinates work
--------------------

In 3D world space, X+ is FORWARD, Y+ is LEFT, and Z+ is UP:

    Z
    |   Y
    |  /
    | /
    |/_____ X

An angle of ZERO radians points directly along the positive X axis; the camera
turns LEFT as this angle increases.

In 2D screen space, X+ is RIGHT, Y+ is DOWN, and Z+ is in front of the camera:

     _____ X
    |
    |
    |
    Y

Note that 2D "screen" space assumes that `x=0,y=0` is the centre of the screen,
with the X axis having a range of -1.0 to + 1.0. The Y axis should be the same,
though is might have a slightly smaller range due to the aspect ratio used.

Limitations
-----------

Currently, only one "turning" angle is supported; this code does not handle the
"look" or "roll" angles that a full 3D engine would have, so any programs using
this code will be restricted to a Doom-style "2.5D" world.
*/

(function () {
  "use strict";

  window.GameCamera = function () {
    this.position = [0, 0, 0];
    // These attributes are READ ONLY; don't try to set them directly!
    this.angle = 0;
    this.sin_a = 0;
    this.cos_a = 1;
  };

  GameCamera.prototype = {
    // Explicitly set the angle value to something:
    setAngle: function(angle) {
      this.angle = angle;
      // Limit the angle to a 0 <= angle < 2pi range:
      while (this.angle <  0        ) { this.angle += Math.PI*2; }
      while (this.angle >= Math.PI*2) { this.angle -= Math.PI*2; }
      // Get the sin/cos values here to avoid recalculating them over and over.
      this.sin_a = Math.sin(this.angle);
      this.cos_a = Math.cos(this.angle);
    },
    // Convert a 3D coordinate into a 2D "screen-space" coordinate:
    screenSpace: function(x, y, z) {
      var p = [];
      // First, translate this coordinate relative to the camera position:
      x -= this.position[0];
      y -= this.position[1];
      z -= this.position[2];
      // Next, transform the 3D coordinate into 2D space and rotate it:
      p[2] = (x * this.cos_a) + (y * this.sin_a);
      p[1] = -z;
      p[0] = (x * this.sin_a) - (y * this.cos_a);
      // Finally, fix this "orthographic" position with perspective correction:
      var d = p[2] > 0.001 ? p[2] : 0.001;
      p[0] /= d;
      p[1] /= d;
      // The end result is an XY coordinate plus a "depth" value for clipping:
      return p;
    }
  };

})();

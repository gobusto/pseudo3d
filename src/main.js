(function () {
  "use strict";

  var player = null;

  function sysInit() {
    sysQuit();
    setUpActorTypes();
    gfxInit();
    player = worldInit();
    // Add event listeners:
    gameInput.keyboard.attach(document.body)
    if (util.ps4.browser()) util.ps4.startPad()
    // Start the main game loop:
    sysMainLoop();
  }

  function sysQuit() {
    // Clean up any old event listeners:
    gameInput.keyboard.clear(document.body)
    if (util.ps4.browser()) util.ps4.stopPad()
  }

  function sysHandleInput() {
    var input = { move: 0, strafe: 0, turn: 0 };

    // Handle joypad inputs, if one is connected:
    var joypad = gameInput.joypad.getState();
    if (util.ps4.browser()) joypad = util.ps4.getPad();

    if (joypad) {
      input['strafe'] = +joypad.axes[0];
      input['move'  ] = -joypad.axes[1];
      input['turn'  ] = -gameInput.joypad.rightStickX(joypad);

      if (joypad.buttons[0].pressed) { input['move'] = +1; }
      if (joypad.buttons[13].pressed) { input['move'] = -1; }
      if (joypad.buttons[4].pressed) { input['strafe'] = -1; }
      if (joypad.buttons[5].pressed) { input['strafe'] = +1; }
      if (joypad.buttons[14].pressed) { input['turn'] = +1; }
      if (joypad.buttons[15].pressed) { input['turn'] = -1; }

      // Prevent jittering if an axis is almost (but not quite) centered:
      Object.keys(input).forEach(function (k) {
        if (input[k] > -0.2 && input[k] < +0.2) { input[k] = 0; }
      });
    }

    // Handle keyboard inputs:
    var keyboard = gameInput.keyboard.getState();
    if (keyboard['W']) { input['move'] = +1; }
    if (keyboard['S']) { input['move'] = -1; }
    if (keyboard['A']) { input['strafe'] = -1; }
    if (keyboard['D']) { input['strafe'] = +1; }
    if (keyboard['ARROWLEFT' ]) { input['turn'] = +1; }
    if (keyboard['ARROWRIGHT']) { input['turn'] = -1; }

    // Update the player state:
    if (player) {
      player.addForce(input['move'] * 0.005, input['strafe'] * 0.005, 0);
      player.turn(input['turn'] * 0.05);
    }
  }

  function sysMainLoop(timer) {
    requestAnimationFrame(sysMainLoop);
    sysHandleInput();
    worldUpdate();
    gfxDraw(player, timer);
  }

  window.onload = sysInit;
})();

(function () {
  "use strict";

  window.actor_sprites = {};

  window.setUpActorTypes = function () {
    actor_sprites = {};
    Array.prototype.forEach.call(document.querySelectorAll(".actor"), function (div) {
      // Load Sprites:
      actor_sprites[div.id] = {};
      Array.prototype.forEach.call(div.querySelectorAll("img"), function (img) {
        actor_sprites[div.id][img.alt] = img;
      });
    });
  };

  // New actors must specify a type, and (optionally) a position/angle.
  window.GameActor = function(kind, x, y, z, angle) {
    this.kind = kind;
    this.state = "idle";
    this.position = [x || 0, y || 0, z || 0];
    this.velocity = [0, 0, 0];
    this.setAngle(angle || 0);
  };

  GameActor.prototype = {
    getSprite: function() {
      var sprite_set = actor_sprites[this.kind];
      return sprite_set && sprite_set[this.state];
    },
    // Get the width, length, and height or an actor:
    getSize: function() {
      var image = this.getSprite();
      if (!image) { return [0.7, 0.7, 0.7]; }
      return [image.width * 0.01, image.width * 0.01, image.height * 0.01];
    },
    // Reposition the actor relative to its current angle/position:
    addForce: function(x, y, z) {
      var sin_a = Math.sin(this.angle);
      var cos_a = Math.cos(this.angle);
      this.velocity[0] += (x * cos_a) + (y * sin_a);
      this.velocity[1] += (x * sin_a) - (y * cos_a);
      this.velocity[2] += z;
    },
    // Update the angle value:
    turn: function(delta) {
      this.setAngle(this.angle + delta);
    },
    // Explicitly set the angle value to something:
    setAngle: function(angle) {
      this.angle = angle;
      // Limit the angle to a 0 <= angle < 2pi range:
      while (this.angle <  0        ) { this.angle += Math.PI*2; }
      while (this.angle >= Math.PI*2) { this.angle -= Math.PI*2; }
    },
    // Update the AI state of this actor:
    update: function () {
      switch (this.kind) {
        case "flame":
          this.timer = ((this.timer || 0) + 1) % (4 * 7);
          this.state = "frame" + String(Math.floor(this.timer / 7));
        break;
        default: break;
      }
    }
  };

})();

(function () {
  "use strict";

  window.game_world = [];
  window.game_objects = [];

  window.worldInit = function () {
    // Set up the map tiles:
    game_world = [
      [1, 1, 1, 1, 1, 1, 1, 1],
      [1, 0, 0, 0, 0, 0, 0, 1],
      [1, 1, 1, 1, 1, 1, 0, 1],
      [1, 0, 0, 0, 0, 0, 0, 1],
      [1, 0, 1, 1, 1, 1, 1, 1],
      [1, 0, 0, 0, 0, 0, 0, 1],
      [1, 0, 0, 0, 1, 0, 0, 1],
      [1, 1, 1, 1, 1, 1, 1, 1]
    ];

    // Set up the map objects:
    game_objects = [];
    game_objects.push(new GameActor("shells", 4.0, 1.0));
    game_objects.push(new GameActor("flame", 6.3, 0.7));
    game_objects.push(new GameActor("barrel", 6.3, 3.3));
    game_objects.push(new GameActor("barrel", 0.7, 2.7));
    game_objects.push(new GameActor("tree",   3.0, 6.0, -0.1));
    game_objects.push(new GameActor("shells", 6.0, 5.5));

    var player = new GameActor("player", 2.0, 1.0);
    game_objects.push(player);
    return player;
  };

  window.worldUpdate = function () {
    game_objects.forEach(function (actor) {
      // Update the actor AI:
      actor.update();
      for (var axis = 0; axis < 3; ++axis) {
        // Update actor position:
        var old_position = actor.position[axis];
        actor.position[axis] += actor.velocity[axis];
        actor.velocity[axis] *= 0.7;
        // Check for collision with walls:
        if (worldCollide(actor)) {
          actor.position[axis] = old_position;
          actor.velocity[axis] = 0;
        }
      }
    });
  }

  // Collision detection. (TODO: This only needs to check NEARBY tiles!)
  function worldCollide(actor) {
    var s = actor.getSize();
    for (var y = 0; y < game_world.length; ++y) {
      for (var x = 0; x < game_world[y].length; ++x) {
        switch (game_world[y][x]) {
          // Empty cells:
          case 0:
          break;
          // Walls:
          default:
            if (actor.position[0] + s[0]/2 > x - 0.5 &&
                actor.position[0] - s[0]/2 < x + 0.5 &&
                actor.position[1] + s[1]/2 > y - 0.5 &&
                actor.position[1] - s[1]/2 < y + 0.5) {
              return game_world[y][x];
            }
          break;
        }
      }
    }
    // Not colliding with any cubes:
    return null;
  }

  window.getTile = function(x, y) {
    if      (y < 0 || y >= game_world.length   ) { return 0; }
    else if (x < 0 || x >= game_world[y].length) { return 0; }
    return game_world[y][x];
  };

})();

Pseudo-3D rendering with the HTML canvas element
================================================

This project is an attempt to render a simple 3D world using a "2D" HTML canvas
context, instead of relying on "true" 3D rendering with WebGL.

Controls
--------

Use WASD to move and the left/right arrow keys to turn. Keyboard input requires
a browser that supports the `event.key` property - Firefox and Chromium are OK.

Joypads are also supported. The following devices have been tested and work for
both Firefox and Chromium:

+ 🎮 Xbox 360 controller
+ 🎮 Playstation 3 SIXAXIS
+ 🎮 Playstation 4 DualShock

Note that these devices were tested with Debian GNU/Linux; on Windows or macOS,
they should work fine with Chrome, but may not function correctly with Firefox.
This is because Chrom(e/ium) automatically detects these particular devices and
uses a "standard" button-mapping for them, whereas Firefox simply uses whatever
the system device driver provides.

TODOs
-----

+ 🌍 Improve the world/actors system.
+ 🎵 Sounds, music, etc.

Thanks
------

+ ♥ The [Freedoom](http://freedoom.github.io) project, for providing the sprites.

Licensing
---------

This code is made available under the [MIT](http://opensource.org/licenses/MIT)
license, which allows you to use or modify it for any purpose including paid-for
and closed-source projects. All I ask in return is that proper attribution is
given (i.e. don't remove my name from the copyright text in the source code, and
perhaps include me on the "credits" screen, if your program has such a thing).
